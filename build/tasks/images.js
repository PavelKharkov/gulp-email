'use strict';

const gulp = require('gulp');
const imagemin = require('gulp-imagemin');
const imageminPngquant = require('imagemin-pngquant');
const plumber = require('gulp-plumber');
const changed = require('gulp-changed');

gulp.task('images', () => {
    return gulp.src(config.paths.src_images + config.paths.ignored_paths + '*')
        .pipe(plumber())
        .pipe(changed(config.paths.dist))
        .pipe(imagemin([
            imagemin.gifsicle(),
            imagemin.jpegtran(),
            imagemin.svgo({
                plugins: [{
                    removeViewBox: false,
                    cleanupIDs: false
                }]
            }),
            imageminPngquant({
                speed: 1,
                quality: 0
            })
        ], {
            verbose: true
        }))
        .pipe(gulp.dest(config.paths.dist));
});