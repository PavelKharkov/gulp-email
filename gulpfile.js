'use strict';

const fs = require('fs');
const path = require('path');

const buildTasks = path.join('build', 'tasks');
require(path.join(__dirname, 'build', 'config.js'));

//Require gulp tasks
fs.readdirSync(buildTasks).filter(file => {
    require(path.join(__dirname, buildTasks, file));
});