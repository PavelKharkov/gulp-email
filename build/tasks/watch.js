'use strict';

const gulp = require('gulp');
const watch = require('gulp-watch');

gulp.task('watch', () => {
    global.isWatch = true;

    watch(config.paths.src_css + config.paths.ignored_paths + '*.' + config.styles_extension, () => {
        gulp.parallel('css')();
    });
    watch(config.paths.src_html + config.paths.ignored_paths + '*.' + config.html_extension, () => {
        gulp.parallel('html')();
    });

    watch(config.paths.src_images + config.paths.ignored_paths + '*.*', () => {
        gulp.parallel('images')();
    });
    watch(config.paths.src_fonts + config.paths.ignored_paths + '*.*', () => {
        gulp.parallel('fonts')();
    });
});