'use strict';

const gulp = require('gulp');

gulp.task('default', () => {
    process.env.NODE_ENV = 'development';

    gulp.parallel('server', 'watch')();
});