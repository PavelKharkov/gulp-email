'use strict';

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

const gulp = require('gulp');
const EmailBuilder = require('email-builder-core');
require('../email-builder-config.js');
const fs = require('fs');

const options = {
    emailTest: {
        from: emailBuilderConfig.email,
        to: emailBuilderConfig.email,
        subject: emailBuilderConfig.subject,

        nodemailer: {
            transporter: {
                service: emailBuilderConfig.service,
                auth: {
                    user: emailBuilderConfig.email,
                    pass: emailBuilderConfig.pass
                }
            }
        }
    }
};
const emailBuilder = new EmailBuilder(options);

gulp.task('mail', done => {
    gulp.series(gulp.parallel('html', 'css'), seriesDone => {
        emailBuilder.inlineCss(config.paths.dev + config.html_file + '.html').then(html => {
            if (!fs.existsSync(config.paths.dist)) fs.mkdirSync(config.paths.dist);
            fs.writeFileSync(config.paths.dist + config.html_file + '.html', html, config.encoding);

            if (process.env.NODE_ENV === 'production') emailBuilder.sendEmailTest(html);

            process.env.NODE_ENV = 'development';
            gulp.parallel('html', 'css')();

            seriesDone();
            done();
        });
    })();
});