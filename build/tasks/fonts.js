'use strict';

const gulp = require('gulp');
const changed = require('gulp-changed');

gulp.task ('fonts', () => {
    return gulp.src(config.paths.src_fonts + config.paths.ignored_paths + '*.*')
        .pipe(changed(config.paths.dist))
        .pipe(gulp.dest(config.paths.dist));
});