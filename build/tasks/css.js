'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass');
const plumber = require('gulp-plumber');
const gulpif = require('gulp-if');
const changed = require('gulp-changed');
const change = require('gulp-change');

gulp.task('css', () => {
    return gulp.src(config.paths.src_css + '*.' + config.styles_extension)
        .pipe(plumber())
        .pipe(gulpif(global.isWatch, changed(config.paths.dev, {extension: '.css'})))
        .pipe(change(content => `$dist: ${!!process.env.NODE_ENV === 'production'};\r\n${content}`))
        .pipe(gulpif(config.styles_extension !== 'css', sass.sync({
            outputStyle: 'nested',
            errLogToConsole: true
        })))
        .pipe(gulp.dest(config.paths.dev));
});

//TODO:fix config vars