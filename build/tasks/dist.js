'use strict';

const gulp = require('gulp');

gulp.task('dist', done => {
    process.env.NODE_ENV = 'production';

    gulp.parallel('mail')();
    done();
});