'use strict';

const gulp = require('gulp');
const pug = require('gulp-pug');
const plumber = require('gulp-plumber');
const gulpif = require('gulp-if');

const change = require('gulp-change');
const pretty = require('pretty');

const prettify = content => pretty(content);

gulp.task('html', () => {
    return gulp.src(config.paths.src_html + config.html_file + '.' + config.html_extension)
        .pipe(plumber())
        .pipe(gulpif(config.html_extension !== 'html', pug({
            locals: {
                dist: process.env.NODE_ENV === 'production'
            }
        })))
        .pipe(change(prettify))
        .pipe(gulp.dest(config.paths.dev));
});