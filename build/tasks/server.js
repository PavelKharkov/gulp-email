'use strict';

const gulp = require('gulp');
const browserSync = require('browser-sync').create();

global.serverTask = () => {
    return browserSync.init({
        server: {
            baseDir: config.server.root,
            routes: {
                '/dist': config.server.dist
            },
            directory: true,
            notify: true,
            ghostMode: false,
            tunnel: config.server.tunnel
        },
        startPath: config.server.start
    });
};

gulp.task('server', () => serverTask());