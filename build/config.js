config = {
    //Main folders
    paths: {
        dist: 'dist/', //Path to distribution folder
        dev: 'dev/', //Path to dev folder
        src: 'src/', //Path to source folder
        build: 'build/', //Path to gulp folder

        ignored_paths: '[^_]' //Regex for files which will be ignored by compilers (default - all files which filename starts with "_")
    },

    //Html precompiled file
    html_file: 'index',

    //Precompiled extensions
    html_extension: 'pug',
    styles_extension: 'scss',

    //Other
    encoding: 'utf8'
};

Object.defineProperties(config.paths, {
    //Src folders paths
    src_config: {value: config.paths.src + 'config/'},
    src_css: {value: config.paths.src + 'css/'},
    src_fonts: {value: config.paths.src + 'fonts/'},
    src_html: {value: config.paths.src + 'html/'},
    src_images: {value: config.paths.src + 'images/'}
});

//Server config
config.server = {
    root: './' + config.paths.dev, //Server root folder
    start: '.', //Starting opened folder
    dist: config.paths.dist, //Dist folder for assets
    tunnel: false //Server tunnel for external access
};